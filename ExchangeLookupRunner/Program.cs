﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExchangeLookup;

namespace ExchangeLookupRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            var todayData = LoadData.LoadRatesFromWeb();
            SaveData.Save(todayData);
            SaveData.SaveToDB(todayData);
            //var yesterdayData = LoadData.LoadRatesFromFile(string.Format("C:\\Intel\\rates_{0}.txt", DateTime.Now.AddDays(-1).ToString("yyyyMMdd")));
            var yesterdayData = LoadData.LoadFromDB();
            var diffData = todayData - yesterdayData;
            //var diffData = Calculate.GetDiff(todayData, yesterdayData);
            Console.WriteLine(diffData);
            Console.ReadKey();
        }
    }
}
