﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeLookup
{
    public class Calculate
    {

        public static RatesData GetDiff(RatesData rates1, RatesData rates2)
        {
            var res = new RatesData();

            res.rates.AUD = rates1.rates.AUD - rates2.rates.AUD;
            res.rates.BGN = rates1.rates.BGN - rates2.rates.BGN;
            res.rates.BRL = rates1.rates.BRL - rates2.rates.BRL;
            res.rates.CAD = rates1.rates.CAD - rates2.rates.CAD;
            res.rates.CHF = rates1.rates.CHF - rates2.rates.CHF;
            res.rates.CNY = rates1.rates.CNY - rates2.rates.CNY;
            res.rates.CZK = rates1.rates.CZK - rates2.rates.CZK;
            res.rates.DKK = rates1.rates.DKK - rates2.rates.DKK;
            res.rates.GBP = rates1.rates.GBP - rates2.rates.GBP;
            res.rates.HKD = rates1.rates.HKD - rates2.rates.HKD;
            res.rates.HRK = rates1.rates.HRK - rates2.rates.HRK;
            res.rates.HUF = rates1.rates.HUF - rates2.rates.HUF;
            res.rates.IDR = rates1.rates.IDR - rates2.rates.IDR;
            res.rates.ILS = rates1.rates.ILS - rates2.rates.ILS;
            res.rates.INR = rates1.rates.INR - rates2.rates.INR;
            res.rates.JPY = rates1.rates.JPY - rates2.rates.JPY;
            res.rates.KRW = rates1.rates.KRW - rates2.rates.KRW;
            res.rates.MXN = rates1.rates.MXN - rates2.rates.MXN;
            res.rates.MYR = rates1.rates.MYR - rates2.rates.MYR;
            res.rates.NOK = rates1.rates.NOK - rates2.rates.NOK;
            res.rates.NZD = rates1.rates.NZD - rates2.rates.NZD;
            res.rates.PHP = rates1.rates.PHP - rates2.rates.PHP;
            res.rates.PLN = rates1.rates.PLN - rates2.rates.PLN;
            res.rates.RON = rates1.rates.RON - rates2.rates.RON;
            res.rates.RUB = rates1.rates.RUB - rates2.rates.RUB;
            res.rates.SEK = rates1.rates.SEK - rates2.rates.SEK;
            res.rates.SGD = rates1.rates.SGD - rates2.rates.SGD;
            res.rates.THB = rates1.rates.THB - rates2.rates.THB;
            res.rates.TRY = rates1.rates.TRY - rates2.rates.TRY;
            res.rates.ZAR = rates1.rates.ZAR - rates2.rates.ZAR;
            res.rates.EUR = rates1.rates.EUR - rates2.rates.EUR;

            return res;
        }
    }
}
