﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExchangeLookup
{
    public class SaveData
    {
        public static void Save(RatesData dataToFile)
        {
            string dir = "C:\\Intel"; // @"C:\Intel"
            string fileName = "rates_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            string file = Path.Combine(dir, fileName); 

            try
            {
                //if (!File.Exists(file))
                File.WriteAllText(file, dataToFile.ToString(), Encoding.UTF8); //how to link to another class? String/List?
            }
            
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void SaveToDB(RatesData dataToDB)
        {
            try
            {
                using (var context = new ProjectEntities())
                {
                    RatesJsonDBSet row = new RatesJsonDBSet();
                    row.Date = Convert.ToDateTime(dataToDB.date);
                    row.Json = dataToDB.Json;
                    
                    context.RatesJsonDBSets.Add(row);
                    context.SaveChanges();
                }                
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
