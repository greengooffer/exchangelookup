﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeLookup
{
    public class RatesData
    {        
        public string Json { get; set; } //from previous json code version !!!
        public bool success { get; set; }
        public int timestamp { get; set; }
        public string _base { get; set; }
        public string date { get; set; }
        public Rates rates { get; set; }

        public override string ToString()
        {
            return Json;
        }

        public RatesData()
        {
            rates = new Rates();
        }

        public class Rates
        {
            public float AED { get; set; }
            public float AFN { get; set; }
            public float ALL { get; set; }
            public float AMD { get; set; }
            public float ANG { get; set; }
            public float AOA { get; set; }
            public float ARS { get; set; }
            public float AUD { get; set; }
            public float AWG { get; set; }
            public float AZN { get; set; }
            public float BAM { get; set; }
            public float BBD { get; set; }
            public float BDT { get; set; }
            public float BGN { get; set; }
            public float BHD { get; set; }
            public float BIF { get; set; }
            public float BMD { get; set; }
            public float BND { get; set; }
            public float BOB { get; set; }
            public float BRL { get; set; }
            public float BSD { get; set; }
            public float BTC { get; set; }
            public float BTN { get; set; }
            public float BWP { get; set; }
            public float BYN { get; set; }
            public float BYR { get; set; }
            public float BZD { get; set; }
            public float CAD { get; set; }
            public float CDF { get; set; }
            public float CHF { get; set; }
            public float CLF { get; set; }
            public float CLP { get; set; }
            public float CNY { get; set; }
            public float COP { get; set; }
            public float CRC { get; set; }
            public float CUC { get; set; }
            public float CUP { get; set; }
            public float CVE { get; set; }
            public float CZK { get; set; }
            public float DJF { get; set; }
            public float DKK { get; set; }
            public float DOP { get; set; }
            public float DZD { get; set; }
            public float EGP { get; set; }
            public float ERN { get; set; }
            public float ETB { get; set; }
            public int EUR { get; set; }
            public float FJD { get; set; }
            public float FKP { get; set; }
            public float GBP { get; set; }
            public float GEL { get; set; }
            public float GGP { get; set; }
            public float GHS { get; set; }
            public float GIP { get; set; }
            public float GMD { get; set; }
            public float GNF { get; set; }
            public float GTQ { get; set; }
            public float GYD { get; set; }
            public float HKD { get; set; }
            public float HNL { get; set; }
            public float HRK { get; set; }
            public float HTG { get; set; }
            public float HUF { get; set; }
            public float IDR { get; set; }
            public float ILS { get; set; }
            public float IMP { get; set; }
            public float INR { get; set; }
            public float IQD { get; set; }
            public float IRR { get; set; }
            public float ISK { get; set; }
            public float JEP { get; set; }
            public float JMD { get; set; }
            public float JOD { get; set; }
            public float JPY { get; set; }
            public float KES { get; set; }
            public float KGS { get; set; }
            public float KHR { get; set; }
            public float KMF { get; set; }
            public float KPW { get; set; }
            public float KRW { get; set; }
            public float KWD { get; set; }
            public float KYD { get; set; }
            public float KZT { get; set; }
            public float LAK { get; set; }
            public float LBP { get; set; }
            public float LKR { get; set; }
            public float LRD { get; set; }
            public float LSL { get; set; }
            public float LTL { get; set; }
            public float LVL { get; set; }
            public float LYD { get; set; }
            public float MAD { get; set; }
            public float MDL { get; set; }
            public float MGA { get; set; }
            public float MKD { get; set; }
            public float MMK { get; set; }
            public float MNT { get; set; }
            public float MOP { get; set; }
            public float MRO { get; set; }
            public float MUR { get; set; }
            public float MVR { get; set; }
            public float MWK { get; set; }
            public float MXN { get; set; }
            public float MYR { get; set; }
            public float MZN { get; set; }
            public float NAD { get; set; }
            public float NGN { get; set; }
            public float NIO { get; set; }
            public float NOK { get; set; }
            public float NPR { get; set; }
            public float NZD { get; set; }
            public float OMR { get; set; }
            public float PAB { get; set; }
            public float PEN { get; set; }
            public float PGK { get; set; }
            public float PHP { get; set; }
            public float PKR { get; set; }
            public float PLN { get; set; }
            public float PYG { get; set; }
            public float QAR { get; set; }
            public float RON { get; set; }
            public float RSD { get; set; }
            public float RUB { get; set; }
            public float RWF { get; set; }
            public float SAR { get; set; }
            public float SBD { get; set; }
            public float SCR { get; set; }
            public float SDG { get; set; }
            public float SEK { get; set; }
            public float SGD { get; set; }
            public float SHP { get; set; }
            public float SLL { get; set; }
            public float SOS { get; set; }
            public float SRD { get; set; }
            public float STD { get; set; }
            public float SVC { get; set; }
            public float SYP { get; set; }
            public float SZL { get; set; }
            public float THB { get; set; }
            public float TJS { get; set; }
            public float TMT { get; set; }
            public float TND { get; set; }
            public float TOP { get; set; }
            public float TRY { get; set; }
            public float TTD { get; set; }
            public float TWD { get; set; }
            public float TZS { get; set; }
            public float UAH { get; set; }
            public float UGX { get; set; }
            public float USD { get; set; }
            public float UYU { get; set; }
            public float UZS { get; set; }
            public float VEF { get; set; }
            public float VND { get; set; }
            public float VUV { get; set; }
            public float WST { get; set; }
            public float XAF { get; set; }
            public float XAG { get; set; }
            public float XAU { get; set; }
            public float XCD { get; set; }
            public float XDR { get; set; }
            public float XOF { get; set; }
            public float XPF { get; set; }
            public float YER { get; set; }
            public float ZAR { get; set; }
            public float ZMK { get; set; }
            public float ZMW { get; set; }
            public float ZWL { get; set; }
        }


        public static RatesData LoadFromJson(string ratesData)
        {
            var rate = JsonConvert.DeserializeObject<RatesData>(ratesData);
            rate._base = "USD";
            rate.Json = ratesData; //присваиваем стрингу Json значение json
            return rate;
        }

        public static RatesData operator -(RatesData rates1, RatesData rates2)
        {
            if (rates1._base != rates2._base)
            {
                throw new Exception("Currency not equal");
            }

            var res = new RatesData();

            res.rates.AUD = rates1.rates.AUD - rates2.rates.AUD;
            res.rates.BGN = rates1.rates.BGN - rates2.rates.BGN;
            res.rates.BRL = rates1.rates.BRL - rates2.rates.BRL;
            res.rates.CAD = rates1.rates.CAD - rates2.rates.CAD;
            res.rates.CHF = rates1.rates.CHF - rates2.rates.CHF;
            res.rates.CNY = rates1.rates.CNY - rates2.rates.CNY;
            res.rates.CZK = rates1.rates.CZK - rates2.rates.CZK;
            res.rates.DKK = rates1.rates.DKK - rates2.rates.DKK;
            res.rates.GBP = rates1.rates.GBP - rates2.rates.GBP;
            res.rates.HKD = rates1.rates.HKD - rates2.rates.HKD;
            res.rates.HRK = rates1.rates.HRK - rates2.rates.HRK;
            res.rates.HUF = rates1.rates.HUF - rates2.rates.HUF;
            res.rates.IDR = rates1.rates.IDR - rates2.rates.IDR;
            res.rates.ILS = rates1.rates.ILS - rates2.rates.ILS;
            res.rates.INR = rates1.rates.INR - rates2.rates.INR;
            res.rates.JPY = rates1.rates.JPY - rates2.rates.JPY;
            res.rates.KRW = rates1.rates.KRW - rates2.rates.KRW;
            res.rates.MXN = rates1.rates.MXN - rates2.rates.MXN;
            res.rates.MYR = rates1.rates.MYR - rates2.rates.MYR;
            res.rates.NOK = rates1.rates.NOK - rates2.rates.NOK;
            res.rates.NZD = rates1.rates.NZD - rates2.rates.NZD;
            res.rates.PHP = rates1.rates.PHP - rates2.rates.PHP;
            res.rates.PLN = rates1.rates.PLN - rates2.rates.PLN;
            res.rates.RON = rates1.rates.RON - rates2.rates.RON;
            res.rates.RUB = rates1.rates.RUB - rates2.rates.RUB;
            res.rates.SEK = rates1.rates.SEK - rates2.rates.SEK;
            res.rates.SGD = rates1.rates.SGD - rates2.rates.SGD;
            res.rates.THB = rates1.rates.THB - rates2.rates.THB;
            res.rates.TRY = rates1.rates.TRY - rates2.rates.TRY;
            res.rates.ZAR = rates1.rates.ZAR - rates2.rates.ZAR;
            res.rates.EUR = rates1.rates.EUR - rates2.rates.EUR;

            res.date = DateTime.Now.ToShortDateString();
            res._base = rates1._base;
            res.Json = JsonConvert.SerializeObject(res);

            return res;
        }
    }
}
