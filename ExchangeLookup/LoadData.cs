﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ExchangeLookup
{
    public class LoadData
    {
        public static RatesData LoadRatesFromWeb()
        {
            string json = new WebClient().DownloadString("http://data.fixer.io/api/latest?access_key=df78d03ee7374ecd374688f1a6b95f0e");
            return RatesData.LoadFromJson(json);
        }

        public static RatesData LoadRatesFromFile(string fileName)
        {
            string json = File.ReadAllText(fileName);
            return RatesData.LoadFromJson(json);
        }

        public static RatesData LoadFromDB()
        {
            try
            {
                using (var context = new ProjectEntities())
                {
                    var row = context.RatesJsonDBSets.FirstOrDefault();
                    string json = row.Json;

                    return RatesData.LoadFromJson(json);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }
    }
}